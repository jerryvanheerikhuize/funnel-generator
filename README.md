## TOC

- [Introduction](#introduction)
- [Features](#features)
- [Feedback](#feedback)
- [Contributors](#contributors)
- [Build Process](#development-process)
- [Build Process](#build-process)

## Introduction

A Funnel generator in open scad.

## Features

A few of the things you can do with this generator:

* customize stem and bowl
* customize curvature
* customizable thickness
* customizable handles

## Dependencies
none 

## Feedback

Feel free to send me feedback on [Email](jvanheerikhuize@gmail.com) or file an issue. Feature requests are always welcome. If you wish to contribute, please contact me.

## Contributors

* Create a feature branch
* Write your code
* Commit regularly (and add ticketnumber to commit message)
* Merge develop in your branch
* Resolve merge conflicts
* Create pull request to merge in 'develop'
* Add reviewers to your pull request
* Merge when you have 2 appprovals (yours included)

### Get started

* Download OpenSCAD https://www.openscad.org/
